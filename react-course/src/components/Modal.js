function Modal({ onCancel, onConfirm }) {
  function cancelHandler() {
    onCancel();
  }

  function confirmHandler() {
    onConfirm();
  }

  return (
    <div className='modal'>
      <p>Are you sure you want to delete this item?</p>
      <div className='actions'>
        <button type='button' className='btn btn--alt' onClick={cancelHandler}>
          Cancel
        </button>
        <button type='button' className='btn' onClick={confirmHandler}>
          Confirm
        </button>
      </div>
    </div>
  );
}

export default Modal;
