import { useState, useEffect } from 'react';

import MeetupList from '../components/meetups/MeetupList';
import classes from './AllMeetups.module.css';

function AllMeetupsPage() {
  const [isLoading, setIsLoading] = useState(true);
  const [loadedMeetups, setLoadedMeetups] = useState([]);

  useEffect(() => {
    async function loadMeetups() {
      const API = 'https://react-getting-started-7ac51-default-rtdb.firebaseio.com/meetups.json';

      try {
        setIsLoading(true);
        const response = await fetch(API);

        if (!response.ok) {
          throw new Error('Something went wrong!');
        }

        const data = await response.json();

        const meetups = [];
        for (const key in data) {
          const meetup = {
            id: key,
            ...data[key]
          };
          meetups.push(meetup);
        }

        setLoadedMeetups(meetups);
        setIsLoading(false);
      } catch (error) {
        console.log(error.message);
      }
    }

    loadMeetups();
  }, []);

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  return (
    <section>
      <h1 className={classes.title}>All Meetups</h1>
      <MeetupList meetups={loadedMeetups} />
    </section>
  );
}

export default AllMeetupsPage;
