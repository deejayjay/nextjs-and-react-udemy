import { useHistory } from 'react-router-dom';

import NewMeetupForm from '../components/meetups/NewMeetupForm';
import classes from './NewMeetup.module.css';

function NewMeetupPage() {
  const history = useHistory();

  async function addMeetupHandler(meetupData) {
    const API = 'https://react-getting-started-7ac51-default-rtdb.firebaseio.com/meetups.json';

    try {
      const response = await fetch(API, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(meetupData)
      });

      if (!response.ok) {
        throw new Error('Could not add new meetup!');
      }

      console.log('New meetup added successfully!');
      history.replace('/');
    } catch (error) {
      console.log(error.message);
    }
  }

  return (
    <section>
      <h1 className={classes.title}>Add New Meetup</h1>
      <NewMeetupForm onAddMeetup={addMeetupHandler} />
    </section>
  );
}

export default NewMeetupPage;
