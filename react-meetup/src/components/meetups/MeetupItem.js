import { useContext } from 'react';

import FavoritesContext from '../../store/favorites-context';
import Card from '../ui/Card';
import classes from './MeetupItem.module.css';

function MeetupItem({ meetup }) {
  const { id, image, title, address, description } = meetup;
  const favoriteCtx = useContext(FavoritesContext);

  const itemIsFavorite = favoriteCtx.itemIsFavorite(id);

  function toggleFavoriteStatusHandler() {
    if (itemIsFavorite) {
      favoriteCtx.removeFavorite(id);
      return;
    }
    favoriteCtx.addFavorite(meetup);
  }

  return (
    <li className={classes.item}>
      <Card>
        <div className={classes.image}>
          <img src={image} alt={title} />
        </div>
        <article className={classes.content}>
          <h3 className={classes.title}>{title}</h3>
          <address>{address}</address>
          <p>{description}</p>
        </article>
        <div className={classes.actions}>
          <button type='button' onClick={toggleFavoriteStatusHandler}>
            {itemIsFavorite ? 'Remove from' : 'Add to'} Favorites
          </button>
        </div>
      </Card>
    </li>
  );
}

export default MeetupItem;
