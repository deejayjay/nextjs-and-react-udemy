import Link from 'next/link';

import classes from './404.module.css';

function NotFound() {
  return (
    <div className={classes.container}>
      <section className={classes.content}>
        <h1 className={classes.title}>Are you lost?</h1>
        <p className={classes.para}>
          Return to&nbsp;
          <Link className={classes.link} href='/'>
            home page
          </Link>
        </p>
      </section>
    </div>
  );
}

export default NotFound;
