import { useRouter } from 'next/router';

import EventSummary from '../../components/event-details/EventSummary';
import EventLogistics from '../../components/event-details/EventLogistics';
import EventContent from '../../components/event-details/EventContent';
import ErrorAlert from '../../components/ui/ErrorAlert';
import { getEventById } from '../../dummy-data';

function EventDetailPage() {
  const router = useRouter();
  const eventId = router.query.eventId;
  const event = getEventById(eventId);

  if (!event) {
    return (
      <ErrorAlert>
        <p>No event found!</p>
      </ErrorAlert>
    );
  }

  return (
    <>
      <EventSummary title={event.title} />
      <EventLogistics event={event}></EventLogistics>
      <EventContent>
        <p>{event.description}</p>
      </EventContent>
    </>
  );
}

export default EventDetailPage;
