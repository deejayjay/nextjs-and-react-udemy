import ErrorIcon from '../icons/ErrorIcon';
import classes from './ErrorAlert.module.css';

function ErrorAlert(props) {
  return (
    <div className={classes.alert}>
      <ErrorIcon />
      {props.children}
    </div>
  );
}

export default ErrorAlert;
