import { useRouter } from 'next/router';

function SelectedClientProjectPage() {
  const router = useRouter();
  const { id, clientProjectId } = router.query;
  console.log({ id, clientProjectId });

  return (
    <>
      <h1>The Project Page for a Specific Project for a Selected Client</h1>
    </>
  );
}

export default SelectedClientProjectPage;
