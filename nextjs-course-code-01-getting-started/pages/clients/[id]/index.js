import { useRouter } from 'next/router';

function ClientProjectsPage() {
  const router = useRouter();
  const { id } = router.query;
  console.log({ id });

  function loadProjectHandler() {
    router.push({
      pathname: '/clients/[id]/[clientProjectId]',
      query: { id: id, clientProjectId: 'projecta' }
    });
  }

  return (
    <>
      <h1>The Projects of a Given Client</h1>
      <button type='button' onClick={loadProjectHandler}>
        Load Project
      </button>
    </>
  );
}

export default ClientProjectsPage;
