import Link from 'next/link';

function ClientsPage() {
  const clients = [
    { id: 'wcb', name: 'WCB' },
    { id: 'goa', name: 'GoA' }
  ];

  return (
    <>
      <h1>Client Page</h1>
      <ul>
        {clients.map((client) => (
          <li key={client.id}>
            <Link
              href={{
                pathname: 'clients/[id]',
                query: { id: client.id }
              }}
            >
              {client.name}
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
}

export default ClientsPage;
