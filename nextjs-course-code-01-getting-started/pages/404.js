function NotFoundPage() {
  return (
    <section className='not-found'>
      <h1 className='not-found__title'>
        <i class='bi bi-exclamation-diamond'></i>
        Page Not Found!!!
      </h1>
    </section>
  );
}

export default NotFoundPage;
