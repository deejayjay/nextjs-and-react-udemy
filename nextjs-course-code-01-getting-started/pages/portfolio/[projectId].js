import { useRouter } from 'next/router';

function PortfolioProject() {
  const router = useRouter();
  const { projectId } = router.query;
  console.log(projectId);

  return (
    <>
      <h1>Portfolio Project Page</h1>
    </>
  );
}

export default PortfolioProject;
